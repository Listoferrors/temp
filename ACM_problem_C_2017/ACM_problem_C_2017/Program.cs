﻿using System;

namespace ACM_problem_C_2017
{
    class Program
    {

        static void Main(string[] args)
        {
            
            string uInput;
            
            int originalAmount = 0;
            int afterAmount = 0;
            
            int[,] array2dDef = new int[2, 3] { { 50, 20, 3 }, { 20, 10, 3 } };
            int[,] array2dDef2 = new int[5, 5] { { 1, 4, 0, 5, 2 }, { 2, 1, 2, 0, 1 }, { 0, 2, 3, 4, 4 }, { 0, 3, 0, 3, 1 }, { 1, 2, 2, 1, 1 } };
            int[,] array2dDef3 = new int[5, 5] { { 4, 8, 7, 2, 0 }, { 5, 6, 7, 3, 1 }, { 1, 2, 3, 2, 1 }, { 4, 3, 2, 1, 5 }, { 6, 0, 10, 2, 2 } };
            int[,] oArray2D;

            Console.Write("Give number of rows: ");
            uInput =  Console.ReadLine();
            switch(uInput)
            {
                case "def":
                    oArray2D = array2dDef;
                    break;
                case "def2":
                    oArray2D = array2dDef2;
                    break;
                case "def3":
                    oArray2D = array2dDef3;
                    break;
                default:
                    int n;
                    bool numeric = int.TryParse(uInput, out n);
                    if(numeric && n != 0)
                    {
                        oArray2D = UserInput(n);
                    }
                    else
                    {
                        oArray2D = array2dDef2;
                    }
                    break;
            }
            
            int[] side = new int[oArray2D.GetLength(0)];
            int[] front = new int[oArray2D.GetLength(1)];
            
            foreach(int n in oArray2D)
            {
                originalAmount += n;
            }
            
            //check what the security cameras see
            for (int i = 0; i < side.Length; i++) 
            {
                int checkRow = 0;
                for (int x = 0; x < front.Length; x++)
                {
                    if(oArray2D[i, x] > front[x])
                    {
                        front[x] = oArray2D[i, x];
                    }
                    if(oArray2D[i, x] > checkRow)
                    {
                        checkRow = oArray2D[i, x];
                    }
                }
                side[i] = checkRow;
                                
            }
            //check if one pile can be seen by both Camera's as the tallest pile per row/col
            int[,] answer = new int[oArray2D.GetLength(0), oArray2D.GetLength(1)];
            bool[] rowDone = new bool[front.Length];
            bool[] colDone = new bool[side.Length];
            for(int i = 0; i < side.Length; i++)
            {
                for(int x = 0; x < front.Length; x++)
                {
                    if (side[i] == front[x] & oArray2D[i,x] != 0)
                    {
                        answer[i, x] = front[x];
                        afterAmount += front[x];
                        rowDone[x] = true;
                        colDone[i] = true;
                    }
                   
                }
            }
            
            //check rest of the rows                     
            for (int i = 0; i < rowDone.Length; i++)
            {
                if(rowDone[i] == false)
                {
                    for(int x = 0; x < colDone.Length; x++)
                    {
                        if (oArray2D[x, i] > 0 && oArray2D[x, i] == front[i])
                        {
                            answer[x, i] = oArray2D[x, i];
                            afterAmount += oArray2D[x, i];
                            rowDone[i] = true;
                            break;
                        }
                    }
                }
                
            }
            //check rest of the columns
            for (int i = 0; i < colDone.Length; i++)
            {
                if (colDone[i] == false)
                {
                    for(int x = 0; x < rowDone.Length; x++)
                    {
                        if(oArray2D[i, x] > 0 && oArray2D[i, x] == side[i])
                        {
                            answer[i, x] = oArray2D[i, x];
                            afterAmount += oArray2D[i, x];
                            colDone[i] = true;
                            break;
                        }
                    }
                }
            }
            
            //fill rest of piles that need to be filled, with one crate
            for (int i = 0; i < oArray2D.GetLength(0); i++)
            {
                for(int x = 0; x < oArray2D.GetLength(1); x++)
                {
                    if(oArray2D[i, x] != 0 & answer[i, x] == 0)
                    {
                        answer[i, x] = 1;
                        afterAmount++;
                    }
                }
            }
            //subtract the new piles amount of crates from the original amount.
            Console.WriteLine("max number that can be stolen: " + (originalAmount - afterAmount));
            for (int i = 0; i < oArray2D.GetLength(0); i++)
            {
                for (int x = 0; x < oArray2D.GetLength(1); x++)
                {
                    Console.Write(answer[i, x]+ " ");
                }
                Console.WriteLine();
            }
            
            Console.ReadLine();
            
        }
        static private int[,] UserInput(int rows)
        {
            Console.Write("Give number of columns: ");
            int cols;
            int.TryParse(Console.ReadLine(), out cols);
            int[,] tempArray = new int[rows, cols];
            Console.WriteLine("Give stacks one row at a time. Separate numbers with whitespace.");
            for (int i = 0; i < rows; i++)
            {
                Console.WriteLine("Row n. "+ (i+1)+":");
                String input = Console.ReadLine();
                String[] split = input.Split(' ');
                for(int x = 0; x < cols; x++)
                {
                    tempArray[i, x] = int.Parse(split[x]);
                }
            }

            return tempArray;
        }
    }
}